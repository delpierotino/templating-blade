<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Film;
use App\Genre;
use File;

class FilmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $films = Film::all();
        $genre = Genre::all();

        return view('film/index', compact('films', 'genre'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $genre = Genre::all();
        
        return view('film/create', compact('genre'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validasi
        $request->validate([
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun' => 'required|min:4|max:4',
            'poster' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'genre_id' => 'required'
        ]);

        $imageName = time().'.'.$request->poster->extension();

        $request->poster->move(public_path('images'), $imageName);

        $films = new Film;

        $films->judul = $request['judul'];
        $films->ringkasan = $request['ringkasan'];
        $films->tahun = $request['tahun'];
        $films->poster = $imageName;
        $films->genre_id = $request['genre_id'];
        $films->save();

        return redirect('film')->with('success', 'New Film Created Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $films = Film::find($id);
        $genre = Genre::all();

        return view('film/show', compact('films', 'genre'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $films = Film::find($id);
        $genre = Genre::all();

        return view('film/edit', compact('films', 'genre'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // validasi
        $request->validate([
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun' => 'required|min:4|max:4',
            'poster' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'genre_id' => 'required'
        ]);

        
        $films = Film::find($id);
        
        if ($request->has('poster')){
            $path = 'images/';
            File::delete($path . $films->poster);
            $imageName = time().'.'.$request->poster->extension();

            $request->poster->move(public_path('images'), $imageName);

            $films->poster = $imageName;
        }

        $films->judul = $request['judul'];
        $films->ringkasan = $request['ringkasan'];
        $films->tahun = $request['tahun'];
        $films->genre_id = $request['genre_id'];
        $films->save();

        return redirect('film')->with('success', 'Film Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Film::destroy($id);

        $films = Film::findorfail($id);
        $films->delete();

        $path = 'images/';
        File::delete($path . $films->poster);
        
        return redirect('film')->with('success','Film Removed Successfully');
    }
}

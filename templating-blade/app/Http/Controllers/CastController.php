<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Cast;

class CastController extends Controller
{
    public function create(){
        return view('cast.create');
    }

    public function store(Request $request){
        $request->validate([
            'nama' => ['required', 'max:45'],
            'umur' => ['required', 'numeric'],
            'bio' => ['required']
        ]);

        // dd($request->all());
        
        // Query Builder 
        // $query = DB::table('casts')->insert([
        //     'nama' => $request['nama'],
        //     'umur' => $request['umur'],
        //     'bio' => $request['bio']
        // ]);

        // ORM
        $casts = new Cast; 
        $casts->nama = $request['nama'];
        $casts->umur = $request['umur'];
        $casts->bio = $request['bio'];
        $casts->save();

        return redirect('cast')->with('success', 'New Cast Created');

    }

    public function index(Request $request){
        // Query Builder
        // $casts = DB::table('casts')->get();

        // ORM
        $casts = Cast::all(); 

        return view('cast.index', compact('casts'));
    }

    public function show($id, Request $request){
        // Query Builder
        // $casts = DB::table('casts')->where('id', $id)->first();

        // ORM
        $casts = Cast::find($id);

        return view('cast.show', compact('casts'));
    }

    public function edit($id){
        // Query Builder
        // $casts = DB::table('casts')->where('id', $id)->first();
        
        // ORM
        $casts = Cast::find($id);

        return view('cast.edit', compact('casts'));
    }

    public function update($id, Request $request){
        $request->validate([
            'nama' => ['required', 'max:45'],
            'umur' => ['required', 'numeric'],
            'bio' => ['required']
        ]);

        // Query Builder
        // $query = DB::table('casts')
        //             ->where('id', $id)
        //             ->update([
        //                 'nama'=>$request['nama'],
        //                 'umur'=>$request['umur'],
        //                 'bio'=>$request['bio']
        //             ]);

        // ORM
        $update = Cast::where('id', $id)->update([
                    'nama'=>$request['nama'],
                    'umur'=>$request['umur'],
                    'bio'=>$request['bio']
        ]); 

        return redirect('cast')->with('success','Cast Updated Successfully');
    }

    public function destroy($id){
        // Query Builder
        // $query = DB::table('casts')->where('id',$id)->delete();

        // ORM
        Cast::destroy($id);
        return redirect('cast')->with('success','Cast Removed Successfully');
    }
}

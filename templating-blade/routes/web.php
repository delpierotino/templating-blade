<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('dashboard', function () {
    return view('page.index');
});

Route::get('table', function () {
    return view('page.tables');
});

Route::get('data-tables', function () {
    return view('page.data-tables');
});

// Route::get('/cast/create', 'CastController@create');
// Route::post('/cast', 'CastController@store');

// Route::get('/cast', 'CastController@index');

// Route::get('/cast/{id}', 'CastController@show');

// Route::get('/cast/{id}/edit', 'CastController@edit');
// Route::put('/cast/{id}', 'CastController@update');

// Route::delete('/cast/{id}', 'CastController@destroy');

Route::resource('cast', 'CastController');

Route::resource('film', 'FilmController');
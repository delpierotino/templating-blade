@extends('layout.master')

@section('title')
    Create
@endsection

@section('judul')
    Create New Film
@endsection

@section('content')
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Let's Create!</h3>
        </div>
        <form action="/film" method="POST" enctype="multipart/form-data">
        @csrf
            <div class="card-body">
                <div class="form-group">
                    <label for="judul">Judul</label>
                    <input type="text" class="form-control" id="judul" name="judul" value="{{ old('judul', '') }}" placeholder="Masukkan Judul">
                    @error('judul')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="ringkasan">Ringkasan</label>
                    <textarea class="form-control" id="ringkasan" name="ringkasan" cols="30" rows="10" placeholder="Masukkan Ringkasan">{{ old('ringkasan', '') }}</textarea>
                    @error('ringkasan')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="tahun">Tahun</label>
                    <input type="text" class="form-control" id="tahun" name="tahun" value="{{ old('tahun', '') }}" placeholder="Masukkan Tahun">
                    @error('tahun')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="poster">Poster</label>
                    <input type="file" class="form-control" id="poster" name="poster">
                    @error('poster')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                {{-- <div class="form-group">
                    <label>Genre</label>
                    @foreach ($genre as $item)
                    <input type="checkbox" class="form-control" id="genre_id" name="genre_id">{{$item->id}}.
                    @endforeach
                    @error('genre_id')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div> --}}
                <div class="form-group">
                    <label>Genre</label>
                    <select name="genre_id" class="form-control">
                        <option value="">--Pilih Genre--</option>
                        @foreach ($genre as $item)
                            <option value="{{$item->id}}">{{$item->nama}}</option>
                        @endforeach
                    </select>
                    @error('genre_id')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
@endsection
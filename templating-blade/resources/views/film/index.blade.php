@extends('layout.master')

@section('title')
    Index
@endsection

@section('judul')
    Data Film
@endsection

@section('content')
    <div class="card">
        @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
        <div class="card-header">
            <a class="btn btn-primary" href="/film/create" role="button">Add New Film</a>
        </div>

        <div class="card-body table-responsive p-0">
            <table class="table">
                <thead align="center">
                    <tr>
                        <th>No</th>
                        <th>Poster</th>
                        <th>Judul</th>
                        <th>Tahun</th>
                        <th>Ringkasan</th>
                        <th>Genre</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody align="center">
                    @forelse ($films as $key => $film)               
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td><img src="{{asset('images/'.$film->poster)}}" width="100px" alt=""></td>
                            <td>{{ $film->judul }}</td>
                            <td>{{ $film->tahun }}</td>
                            <td>{{ Str::limit($film->ringkasan, 175) }}</td>
                            <td>
                                @foreach ($genre as $item)
                                    @if ($item->id === $film->genre_id)
                                        {{$item->nama}}
                                    @endif
                                @endforeach
                            </td>
                            <td style="display: inline-flex">
                                <a href="/film/{{$film->id}}" class="btn btn-outline-primary btn-sm mr-2"><i class="bi bi-eye-fill"></i></a>
                                <a href="/film/{{$film->id}}/edit" class="btn btn-success btn-sm mr-2">Edit</a>
                                <form action="/film/{{$film->id}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" value="Delete" class="btn btn-danger btn-sm"><i class="bi bi-trash3"></i></button>
                                </form>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="5" align="center">No Data</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
@endsection
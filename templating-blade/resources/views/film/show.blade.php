@extends('layout.master')

@section('title')
    Show
@endsection

@section('judul')
    Detail Film - {{ $films->judul }} 
@endsection

@section('content')
    <div class="card mb-3">
        <div class="row no-gutters">
            <div class="col-md-4 mr-n5">
            <img src="{{asset('images/'.$films->poster)}}" width="300px" alt="...">
            </div>
            <div class="col-md-8 ml-n5">
            <div class="card-body">
                <h3 class="mb-3">{{$films->judul}} ({{$films->tahun}})</h3>
                <p class="card-text font-weight-lighter">{{$films->ringkasan}}</p>
                <p class="card-text"><small class="text-muted">Last updated {{$films->updated_at}}</small></p>
                <span class="badge badge-primary">
                    @foreach ($genre as $item)
                        @if ($item->id === $films->genre_id)
                            {{$item->nama}}
                        @endif
                    @endforeach
                </span>
            </div>
            </div>
        </div>
    </div>
@endsection
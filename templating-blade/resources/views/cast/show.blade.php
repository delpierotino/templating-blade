@extends('layout.master')

@section('title')
    Show
@endsection

@section('judul')
    Detail Cast - {{ $casts->nama }}
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            {{$casts->nama}}
        </div>
        <div class="card-body">
            <p>{{$casts->umur}} tahun</p>
            <footer class="blockquote-footer"><cite title="Source Title">{{$casts->bio}}</cite></footer>
            </blockquote>
        </div>
    </div>
@endsection
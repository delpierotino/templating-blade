@extends('layout.master')

@section('title')
    Index
@endsection

@section('judul')
    Data Cast
@endsection

@section('content')
    <div class="card">
        @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
        <div class="card-header">
            <a class="btn btn-primary" href="/cast/create" role="button">Add New Cast</a>
        </div>

        <div class="card-body table-responsive p-0">
            <table class="table">
                <thead align="center">
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Umur</th>
                        <th>Bio</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody align="center">
                    @forelse ($casts as $key => $cast)
                        <tr>
                            <td width="2%">{{ $key + 1 }}</td>
                            <td width="20%">{{ $cast->nama }}</td>
                            <td width="8%">{{ $cast->umur }} tahun</td>
                            <td width="50%">{{ $cast->bio }}</td>
                            <td style="display: inline-flex">
                                <a href="/cast/{{$cast->id}}" class="btn btn-outline-primary btn-sm mr-2"><i class="bi bi-eye-fill"></i></a>
                                <a href="/cast/{{$cast->id}}/edit" class="btn btn-success btn-sm mr-2">Edit</a>
                                <form action="/cast/{{$cast->id}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" value="Delete" class="btn btn-danger btn-sm"><i class="bi bi-trash3"></i></button>
                                </form>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="5" align="center">No Data</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
@endsection